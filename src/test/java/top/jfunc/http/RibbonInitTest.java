package top.jfunc.http;

import com.netflix.loadbalancer.BaseLoadBalancer;
import com.netflix.loadbalancer.RandomRule;
import com.netflix.loadbalancer.Server;
import org.junit.BeforeClass;
import org.junit.Test;
import top.jfunc.http.component.ribbon.RibbonInit;
import top.jfunc.http.config.Config;
import top.jfunc.http.interceptor.LoadBalancerInterceptor;
import top.jfunc.http.request.DefaultRequest;
import top.jfunc.http.request.HttpRequest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class RibbonInitTest {
    @BeforeClass
    public static void init(){
        List<Server> servers1 = Arrays.asList(new Server("192.168.0.1", 9090), new Server("192.168.0.2", 9090));
        List<Server> servers2 = Arrays.asList(new Server("192.168.1.1",9090), new Server("192.168.1.2",9090));
        RibbonInit.init("xx",servers1);
        RibbonInit.init("yy",servers2, new RandomRule(), null);
    }
    @Test
    public void testXx() throws IOException {
        String clientName = "xx";

        test(clientName);
    }
    @Test
    public void testYy() throws IOException {
        String clientName = "yy";

        test(clientName);
    }
    @Test
    public void testLoadBalancerInterceptor() throws IOException {
        HttpRequest httpRequest = DefaultRequest.of("http://xx/xx/sdas?Sdasda=s&a=asdasd");
        httpRequest.setConfig(Config.defaultConfig());
        HttpRequest request = new LoadBalancerInterceptor().onBefore(httpRequest);
        System.out.println(request.getCompletedUrl());

    }

    private static void test(String clientName) {
        BaseLoadBalancer loadBalancer = RibbonInit.get(clientName);
        System.out.println(loadBalancer.getAllServers());

        System.out.println(loadBalancer.getReachableServers());

        Server server1 = loadBalancer.chooseServer();
        System.out.println(server1);
        Server server2 = loadBalancer.chooseServer();
        System.out.println(server2);
        Server server3 = loadBalancer.chooseServer();
        System.out.println(server3);
    }
}
