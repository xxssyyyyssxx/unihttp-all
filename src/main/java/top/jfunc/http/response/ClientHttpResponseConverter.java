package top.jfunc.http.response;

import java.io.IOException;

/**
 * 结果封装，更通用的封装方式，输入更通用，自己转化成需要的数据结构string、bytes、file...
 * 必须立即使用，因为框架会自动关闭{@link ClientHttpResponse}
 * 常见的几个要素用{@link top.jfunc.http.util.ResponseExtractor}即可获取
 * @author xiongshiyan at 2020/11/30 , contact me with email yanshixiong@126.com or phone 15208384257
 */
@FunctionalInterface
public interface ClientHttpResponseConverter<R> {
    /**
     * 是否支持流式读取，如果要支持，则框架不会自动关闭资源，需要调用者完成资源关闭
     * 在有些场景下，可能需要流式读取，比如获取文件流，此时需要返回true
     * @return 是否支持stream
     */
    default boolean supportStream() {
        return false;
    }

    /**
     * 将响应转换为需要的
     * @param clientHttpResponse ClientHttpResponse
     * @param resultCharset 字符集
     * @return R what you want
     * @throws IOException IOException
     */
    R convert(ClientHttpResponse clientHttpResponse, String resultCharset) throws IOException;
}
